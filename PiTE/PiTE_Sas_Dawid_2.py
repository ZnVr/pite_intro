import random
import time

# Microwave states
states = ["empty_closed", "empty_open", "full_open", "full_closed", "working",
    "finished"]


class Microwave:

    def __init__(self):
        self.state = "empty_closed"
        self.timer = 0

    def open(self):
        if(self.state == "empty_closed"):
            self.state = "empty_open"
        if(self.state == "full_closed"):
            self.state = "full_open"
        elif(self.state == "finished"):
            self.state = "full_open"

    def close(self):
        if(self.state == "empty_open"):
            self.state = "empty_closed"
        if(self.state == "full_open"):
            self.state = "full_closed"

    def insert(self):
        if(self.state == "empty_open"):
            self.state = "full_open"

    def pullout(self):
        if(self.state == "full_open"):
            self.state = "empty_open"

    def start(self, sec):
        if(self.state == "full_closed"):
            self.state = "working"
            self.timer = sec

    # If microwave is working, its timer is running
    # When time runs out: change state to "finished"
    def timepass(self):
        if(self.timer > 0):
            self.timer = self.timer - 1
        else:
            if(self.state == "working"):
                self.state = "finished"

# Print header
print(" " * 5 + "State\tAction\tResult")
print(" " * 5 + '-' * 35)

# Available decisions
inputs = ["open", "close", "insert", "pullout", "start"]
# Number of attempts and a counter
attempts = 100
i = 1

mw = Microwave()
while(i <= attempts):
    prev_state = mw.state
    # Select a decision randomly
    user_input = random.choice(inputs)
    # Invoke corresponding method
    if(user_input != "start"):
        getattr(mw, user_input)()
    else:
        getattr(mw, user_input)(random.randint(1, 5))

    # Display info about state, input and output
    timer = ""
    if(mw.state == "working"):
        timer = str(mw.timer) + " sec"
    print("#" + "{:0>3}".format(i) + " " + prev_state + "\t" + user_input +
        "\t" + mw.state + "   " + timer)

    # Simulate time intervals
    mw.timepass()
    i = i + 1
    time.sleep(0.1)