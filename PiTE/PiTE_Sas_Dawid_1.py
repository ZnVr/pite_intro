import os


def dir_capacity(path, dirc):
    '''Returns capacity of a directory "dirc" situated in path "path"'''

    capacity = 0
    # List files in specified directory
    files = os.listdir(path + os.sep + dirc)
    # Recursively traverse through directory structure and increase capacity
    for f in files:
        f_abspath = path + os.sep + dirc + os.sep + f
        if(os.path.isfile(f_abspath)):
            capacity = capacity + os.stat(f_abspath).st_size
        else:
            capacity = capacity + dir_capacity(path, dirc + os.sep + f)
    return capacity


def max_dir(path):
    """Returns the largest directory in path 'path'"""

    # Directory list
    dirs = [d for d in os.listdir(path) if not os.path.isfile(path + os.sep +
        d)]

    # Directory sizes list
    dirs_sizes = [dir_capacity(path, dirc) for dirc in dirs]

    # Tuple (dir : size)
    dirs_tuples = zip(dirs, dirs_sizes)

    # Display list of directories ordered by their size
    dirs_tuples = sorted(dirs_tuples, key=lambda dir_tuple: dir_tuple[1],
        reverse=True)
    for t in dirs_tuples:
        print(t[0] + "{:.>{width}}".format(t[1], width=64 - len(t[0])) + " B")

if(__name__ == "__main__"):
    max_dir('.')