﻿Repozytorium zawiera skrypty z pierwszych zajęć.

Folder 'PiTE' zawiera dwa skrypty:
 1.Wyświetla strukturę folderów w zadanym katalogu. Aby dokonać specyfikacji katalogu należy zmodyfikować ostatnią linijkę skryptu: max_dir('nazwa_folderu'). Domyślnie jest to folder bieżący.
 2.Symulacja maszyny stanów

Folder 'PiTE_tests' zawiera unittesty maszyny stanów. Aby wyświetlić zbiorczy wynik testu należy uruchomić skrypt microwave_test_manager.py