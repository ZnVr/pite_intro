import unittest
import Microwave


class TestMicrowaveInsert(unittest.TestCase):

    def setUp(self):
        self.mw = Microwave.Microwave()

    def test_insert(self):
        self.mw.state = 'empty_closed'
        self.mw.insert()
        self.assertEqual('empty_closed', self.mw.state)

        self.mw.state = 'full_closed'
        self.mw.insert()
        self.assertEqual('full_closed', self.mw.state)

        self.mw.state = 'finished'
        self.mw.insert()
        self.assertEqual('finished', self.mw.state)

        self.mw.state = 'empty_open'
        self.mw.insert()
        self.assertEqual('full_open', self.mw.state)

        self.mw.state = 'full_open'
        self.mw.insert()
        self.assertEqual('full_open', self.mw.state)

        self.mw.state = 'working'
        self.mw.insert()
        self.assertEqual('working', self.mw.state)


if(__name__ == '__main__'):
    unittest.main()