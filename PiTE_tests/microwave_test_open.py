import unittest
import Microwave


class TestMicrowaveOpen(unittest.TestCase):

    def setUp(self):
        self.mw = Microwave.Microwave()

    def test_open(self):
        self.mw.state = 'empty_closed'
        self.mw.open()
        self.assertEqual('empty_open', self.mw.state)

        self.mw.state = 'full_closed'
        self.mw.open()
        self.assertEqual('full_open', self.mw.state)

        self.mw.state = 'finished'
        self.mw.open()
        self.assertEqual('full_open', self.mw.state)

        self.mw.state = 'empty_open'
        self.mw.open()
        self.assertEqual('empty_open', self.mw.state)

        self.mw.state = 'full_open'
        self.mw.open()
        self.assertEqual('full_open', self.mw.state)

        self.mw.state = 'working'
        self.mw.open()
        self.assertEqual('working', self.mw.state)


if(__name__ == '__main__'):
    unittest.main()