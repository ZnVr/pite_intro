import unittest
import Microwave


class TestMicrowaveStart(unittest.TestCase):

    def setUp(self):
        self.mw = Microwave.Microwave()

    def test_start(self):
        self.mw.state = 'empty_closed'
        self.mw.start(1)
        self.assertEqual('empty_closed', self.mw.state)

        self.mw.state = 'full_closed'
        self.mw.start(1)
        self.assertEqual('working', self.mw.state)

        self.mw.state = 'finished'
        self.mw.start(1)
        self.assertEqual('finished', self.mw.state)

        self.mw.state = 'empty_open'
        self.mw.start(1)
        self.assertEqual('empty_open', self.mw.state)

        self.mw.state = 'full_open'
        self.mw.start(1)
        self.assertEqual('full_open', self.mw.state)

        self.mw.state = 'working'
        self.mw.start(1)
        self.assertEqual('working', self.mw.state)


if(__name__ == '__main__'):
    unittest.main()