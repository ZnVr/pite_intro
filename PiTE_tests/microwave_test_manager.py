import unittest

import microwave_test_open as test_open
import microwave_test_close as test_close
import microwave_test_insert as test_insert
import microwave_test_pullout as test_pullout
import microwave_test_start as test_start

# Instantiating particular TestCases
test1 = test_open.TestMicrowaveOpen('test_open')
test2 = test_close.TestMicrowaveClose('test_close')
test3 = test_insert.TestMicrowaveInsert('test_insert')
test4 = test_pullout.TestMicrowavePullout('test_pullout')
test5 = test_start.TestMicrowaveStart('test_start')

# Organizing TestCases into a TestSuite
tests = (test1, test2, test3, test4, test5)
suite = unittest.TestSuite(tests)

# Performing tests
result = unittest.TestResult()
suite.run(result)

# Interpreting results
print("Number of tests run:", result.testsRun)
print(len(result.errors), "errors:", result.errors)
print(len(result.failures), "Failures:", result.failures)
print("Was the test successful?", result.wasSuccessful())

input("\nPress any key ...")
