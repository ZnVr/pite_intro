# Microwave states
states = ["empty_closed", "empty_open", "full_open", "full_closed", "working",
    "finished"]


class Microwave:

    def __init__(self):
        self.state = "empty_closed"
        self.timer = 0

    def open(self):
        if(self.state == "empty_closed"):
            self.state = "empty_open"
        if(self.state == "full_closed"):
            self.state = "full_open"
        if(self.state == "finished"):
            self.state = "full_open"

    def close(self):
        if(self.state == "empty_open"):
            self.state = "empty_closed"
        if(self.state == "full_open"):
            self.state = "full_closed"

    def insert(self):
        if(self.state == "empty_open"):
            self.state = "full_open"

    def pullout(self):
        if(self.state == "full_open"):
            self.state = "empty_open"

    def start(self, sec):
        if(self.state == "full_closed"):
            self.state = "working"
            self.timer = sec

    # If microwave is working, its timer is running
    # When time runs out: change state to "finished"
    def timepass(self):
        if(self.timer > 0):
            self.timer = self.timer - 1
        else:
            if(self.state == "working"):
                self.state = "finished"