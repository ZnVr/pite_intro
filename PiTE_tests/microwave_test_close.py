import unittest
import Microwave


class TestMicrowaveClose(unittest.TestCase):

    def setUp(self):
        self.mw = Microwave.Microwave()

    def test_close(self):
        self.mw.state = 'empty_closed'
        self.mw.close()
        self.assertEqual('empty_closed', self.mw.state)

        self.mw.state = 'full_closed'
        self.mw.close()
        self.assertEqual('full_closed', self.mw.state)

        self.mw.state = 'finished'
        self.mw.close()
        self.assertEqual('finished', self.mw.state)

        self.mw.state = 'empty_open'
        self.mw.close()
        self.assertEqual('empty_closed', self.mw.state)

        self.mw.state = 'full_open'
        self.mw.close()
        self.assertEqual('full_closed', self.mw.state)

        self.mw.state = 'working'
        self.mw.close()
        self.assertEqual('working', self.mw.state)


if(__name__ == '__main__'):
    unittest.main()