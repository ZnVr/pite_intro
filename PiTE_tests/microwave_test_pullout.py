import unittest
import Microwave


class TestMicrowavePullout(unittest.TestCase):

    def setUp(self):
        self.mw = Microwave.Microwave()

    def test_pullout(self):
        self.mw.state = 'empty_closed'
        self.mw.pullout()
        self.assertEqual('empty_closed', self.mw.state)

        self.mw.state = 'full_closed'
        self.mw.pullout()
        self.assertEqual('full_closed', self.mw.state)

        self.mw.state = 'finished'
        self.mw.pullout()
        self.assertEqual('finished', self.mw.state)

        self.mw.state = 'empty_open'
        self.mw.pullout()
        self.assertEqual('empty_open', self.mw.state)

        self.mw.state = 'full_open'
        self.mw.pullout()
        self.assertEqual('empty_open', self.mw.state)

        self.mw.state = 'working'
        self.mw.pullout()
        self.assertEqual('working', self.mw.state)


if(__name__ == '__main__'):
    unittest.main()